import firebase from 'firebase'
import { ref, onUnmounted } from 'vue'

const config = {
    apiKey: "AIzaSyBayvu8kBLeopUHHBkFGcCWl9VqRKLWv6E",
    authDomain: "fir-testwithvue.firebaseapp.com",
    projectId: "fir-testwithvue",
    storageBucket: "fir-testwithvue.appspot.com",
    messagingSenderId: "1098926522563",
    appId: "1:1098926522563:web:fb2071d2d70831d596a276",
    measurementId: "G-6G08HZFPNF"
}

const firebaseapp = firebase.initializeApp(config)

const db = firebaseapp.firestore()
const userCollection = db.collection('User')

export const createUser = user => {
    return userCollection.add(user)
}

export const getUser = async id => {
    const user = await userCollection.doc(id).get()
    return user.exists ? user.data() : null
  }

  export const updateUser = (id, user) => {
    return userCollection.doc(id).update(user)
  }

  export const deleteUser = id => {
    return userCollection.doc(id).delete()
  }

  export const filterUsers = (filterBy,filter) => {
    console.log(filterBy +' ' + filter)
    const users = ref([])
    const close = userCollection
      .where(filterBy,'==',filter)
      .onSnapshot(snapshot => {
      users.value = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))
    })
    onUnmounted(close)
    return users
  }

  export const useLoadUsers = () => {
    const users = ref([])
    const close = userCollection.onSnapshot(snapshot => {
      users.value = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))
    })
    onUnmounted(close)
    return users
  }